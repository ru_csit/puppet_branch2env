#!/bin/env python
# -*- coding: utf-8 -*-

"""Synchronizes a remote GIT repository such that each remote branch is created as a separate local directory.

This code builds significantly on ruby code written by Adrien Thebo of Puppet Labs.
His code can be found at:
http://puppetlabs.com/blog/git-workflow-and-puppet-environments/



Licensed under the MIT License (MIT)

Copyright (C) <2013> <Stefán Freyr Stefánsson>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to 
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
of the Software, and to permit persons to whom the Software is furnished to do 
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
"""

import os, sys, shutil, optparse, subprocess, signal
import BaseHTTPServer, SimpleHTTPServer, cgi
import json


GIT_CMD = "/usr/bin/git"

# All environments will reside within this directory
ENVIRONMENT_BASEDIR = "/etc/puppet/environments"

# The remote GIT repository to pull from
SOURCE_REPOSITORY = "git@bitbucket.org:ru_csit/puppet_environments.git"


def env_update( branch, delete=False, verbose=False ):
    """Updates the branch environment. If delete is True the local environment will be deleted.
    Otherwise it will either be updated (with "git fetch") if a local environment already exists
    or created (with "git clone REPO_URL --branch branch") if it doesn't.
    """
    if not os.path.isdir(ENVIRONMENT_BASEDIR):
        print( "Environment directory does not exist: " + ENVIRONMENT_BASEDIR )
        return -1

    env_path = "%s/%s" % (ENVIRONMENT_BASEDIR, branch)

    if delete:
        print "Deleting environment: " + env_path
        if os.path.isdir(env_path):
            shutil.rmtree(env_path)
        else:
            print "ERROR: Could not delete environment (not a directory)!"
            return 1
    else:
        if os.path.isdir(env_path):
            print "Updating existing environment: " + env_path
            retval = subprocess.call([GIT_CMD, "fetch", "--all"], cwd=env_path)
            if verbose: print( "git fetch return value: %d" % retval)
            retval = subprocess.call([GIT_CMD, "reset", "--hard", "origin/%s" % branch], cwd=env_path)
            if verbose: print( "git reset return value: %d" % retval)
        elif not os.path.exists(env_path):
            print "Creating new environment: " + env_path
            retval = subprocess.call([GIT_CMD, "clone", SOURCE_REPOSITORY, env_path, "--branch", branch], cwd=ENVIRONMENT_BASEDIR)
            if verbose: print( "git clone return value: %d" % retval)
        else:
            print "ERROR: Could not create/update environment: " + env_path
            return 1
    return 0
        

def env_sync( verbose=False ):
    """Attempts to synchronize the local environments with the remote repository.
    This includes:
      1) deleting any environment that exists locally (as a directory) but doesn't exist remotely (as a branch).
      2) creating (git clone) any environment that exists remotely (as a branch) but doesn't exist locally (as a directory).
      3) updating (git fetch) any environment that exists both remotely (as a branch) and locally (as a directory).
    """
    if not os.path.isdir(ENVIRONMENT_BASEDIR):
        print( "Environment directory does not exist: " + ENVIRONMENT_BASEDIR )
        return -1

    # Create a set that includes the names of all local environments (directories)
    environments = set([])
    entries = os.listdir(ENVIRONMENT_BASEDIR)
    for entry in entries:
        path = os.path.join(ENVIRONMENT_BASEDIR, entry)
        if os.path.isdir(path):
            environments.add(entry)
    p = subprocess.Popen([GIT_CMD, "ls-remote", "--heads", SOURCE_REPOSITORY], stdout=subprocess.PIPE)
    out, err = p.communicate()

    # Create a set that includes the names of all remote branches.
    rbranches = set([line[line.rindex('/')+1:] for line in out.splitlines()])

    # Delete the environments that do not exist upstream
    exist_only_locally = environments - rbranches
    for env in exist_only_locally:
        env_update(env, True, verbose)

    # Create/update all environments that exist remotely
    for env in rbranches:
        env_update(env, False, verbose)


class BitBucketHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    """An HTTP request handler that will handle BitBucket POST service hook events.
    """

    def do_POST(self):
        """Handles the POST requests.
        """
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write("Thanks dude! We're good!\n")

        ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))
        if ctype == 'multipart/form-data':
            postvars = cgi.parse_multipart(self.rfile, pdict)
        elif ctype == 'application/x-www-form-urlencoded':
            length = int(self.headers.getheader('content-length'))
            postvars = cgi.parse_qs(self.rfile.read(length), keep_blank_values=1)
        else:
            postvars = {}

        # Retrieve the affected branches from the POST data
        data = json.loads(postvars['payload'][0])
        commits = data['commits']
        branches = set()
        for commit in commits:
            if 'branch' in commit.keys():
                branches.add(commit['branch'])
            if 'branches' in commit.keys():
                [ branches.add(branch) for branch in commit['branches'] ]

        if not branches:
            # No branches probably means that a branch got deleted so we do a full sync
            env_sync()
        else:
            # Otherwise we loop through the affected branches and update them
            for branch in branches:
                env_update(branch)


def run_webserver(port, server_class=BaseHTTPServer.HTTPServer,
        handler_class=BitBucketHandler):
    print "Starting webserver on port %d" % port
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()


def sigint_handler(signal, frame):
    print "\nGoodbye."
    sys.exit(0)


def main():
    global ENVIRONMENT_BASEDIR, SOURCE_REPOSITORY, GIT_CMD
    signal.signal(signal.SIGINT, sigint_handler)

    
    parser = optparse.OptionParser()
    parser.add_option("-b", "--branch", help="name of git branch to create/update/delete as an environment")
    parser.add_option("-D", "--delete", help="delete branch environment (only relevant with --branch)", action="store_true", default=False)
    parser.add_option("-S", "--fullsync", help="do a full sync and download missing environments and delete non-existing ones", action="store_true", default=False)
    parser.add_option("-w", "--webserver", help="run as a webserver and listen for bitbucket.org POST service messages", action="store_true")
    parser.add_option("-p", "--port", help="run the webserver on the specified port", type=int)
    parser.add_option("--envbase", help="set the base directory for the environments", default=ENVIRONMENT_BASEDIR)
    parser.add_option("--gitrepo", help="set the git repository to work on", default=SOURCE_REPOSITORY)
    parser.add_option("--gitcmd", help="specify the path to the git command", default=GIT_CMD)
    parser.add_option("-v", "--verbose", help="increase output verbosity", action="store_true", default=False)
    (opts, args) = parser.parse_args()

    # Handle stupidity
    if not (opts.branch or opts.webserver or opts.fullsync):
        print "ERROR: One of --branch, --fullsync or --webserver must be specified!"
        sys.exit(-1)
    elif opts.delete and not opts.branch:
        print "ERROR: A branch must be specified to be deleted!"
        sys.exit(-1)
    elif opts.port and not opts.webserver:
        print "ERROR: Specifying a port doesn't make sense if not running a webserver!"
        sys.exit(-1)
    elif opts.fullsync and (opts.branch or opts.delete):
        print "ERROR: Doing a full sync doesn't make sense if --branch or --delete is specified!"
        sys.exit(-1)

    # Handle overrides
    if opts.envbase:
        ENVIRONMENT_BASEDIR = opts.envbase
    if opts.gitrepo:
        SOURCE_REPOSITORY = opts.gitrepo
    if opts.gitcmd:
        GIT_CMD = opts.gitcmd

    if opts.webserver:
        if not opts.port:
            opts.port = 4242

        run_webserver(opts.port)
        print "ERROR: Webserver has stopped unexpectedly."
        sys.exit(-1)

    if opts.branch:
        env_update(opts.branch, opts.delete, opts.verbose)
    elif opts.fullsync:
        env_sync(opts.verbose)

if __name__ == "__main__":
    main()
